const { hostname, protocol } = window.location;
const wpRestRoot = '/?rest_route=/wp/v2';
const wpPort = '9673';
const restRoute = `${protocol}//${hostname}:${wpPort}${wpRestRoot}`;

export default function getMenus(menuSlug = '') {
  return new Promise((resolve, reject) => {
    fetch(`${restRoute}/menus`)
      .then(async (response) => {
        const menus = await response.json();
        const thisMenu = menus[menuSlug];
        if (thisMenu === undefined) {
          reject(Error('Menu not found'));
        } else {
          resolve(thisMenu);
        }
      });
  });
}
