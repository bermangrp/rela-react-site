import '@babel/polyfill';
import React from 'react';
import { render } from 'react-dom';
import DocumentIndex from './document-constructor';
import WebApp from './web-app';

if ('serviceWorker' in navigator && process.env.NODE_ENV === 'production') {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js').then((registration) => {
      console.log('SW registered: ', registration);
    }).catch((registrationError) => {
      console.log('SW registration failed: ', registrationError);
    });
  });
}

render(
  <WebApp />,
  DocumentIndex(),
);
