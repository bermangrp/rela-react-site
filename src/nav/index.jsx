import React, { Component } from 'react';
import {
  Link,
} from 'react-router-dom';
import { PageContext } from '../page-context';
import getMenus from '../wp-api';

export default class Nav extends Component {
  constructor() {
    super();
    this.state = { menuData: [] };
  }

  async componentDidMount() {
    try {
      const mainMenu = await getMenus('main-menu');
      this.setState({ menuData: mainMenu });
    } catch (e) {
      console.warn(e);
    }
  }

  render() {
    return (
      <nav>
        <PageContext.Consumer>
          {(props) => {
            console.log('nav', props);
            console.log('nav', this.state);
            return (
              <ul>
                <li>
                  <Link to="/home">Home</Link>
                  <ul>
                    <li>
                      <Link to="/home/rendering">Rendering with React</Link>
                    </li>
                    <li>
                      <Link to="/home/components">Components</Link>
                    </li>
                    <li>
                      <Link to="/home/props-v-state">Props v. State</Link>
                    </li>
                  </ul>
                </li>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <Link to="/topics">Topics</Link>
                </li>
              </ul>
            );
          }}
        </PageContext.Consumer>
      </nav>
    );
  }
}
