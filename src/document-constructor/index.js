export default (identifier = 'root') => {
  const element = document.createElement('div');
  element.setAttribute('id', identifier);
  element.classList.add(identifier);
  document.body.appendChild(element);
  return element;
};
