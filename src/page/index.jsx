import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import {
  Route,
  Link,
  Switch,
} from 'react-router-dom';

const Subpage = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
);

Subpage.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
};

export default function Pages({ match, location }) {
  return (
    <div>
      <h2>Pages</h2>
      <ul>
        <li>
          <Link to={`${match.url}/rendering`}>Rendering with React</Link>
        </li>
        <li>
          <Link to={`${match.url}/components`}>Components</Link>
        </li>
        <li>
          <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
        </li>
      </ul>

      <TransitionGroup>
        <CSSTransition key={location.pathname} classNames="fade" timeout={300}>
          <Switch location={location}>
            <Route path={`${match.url}/:topicId`} component={Subpage} />
            <Route
              exact
              path={match.url}
              render={() => <h3>Please select a topic.</h3>}
            />
          </Switch>
        </CSSTransition>
      </TransitionGroup>
    </div>
  );
}

Pages.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};
