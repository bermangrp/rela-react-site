import React, { Component } from 'react';
import {
  Redirect,
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import {
  pages,
  PageContext,
} from '../page-context';
import Nav from '../nav';

const getRouteList = () => {
  const routeList = [];
  const pageSlugs = Object.keys(pages);
  const firstRoute = pages[pageSlugs[0]];

  routeList.push(
    <Route
      exact
      path={pageSlugs[0]}
      key={pageSlugs[0]}
      component={firstRoute.component}
    />,
  );

  pageSlugs.forEach((page) => {
    routeList.push(
      <Route
        path={page}
        key={page}
        component={pages[page].component}
      />,
    );
  });

  routeList.push(
    <Redirect
      exact
      key="redirect"
      from="/"
      to={firstRoute.slug}
    />,
  );

  routeList.push(
    <Redirect
      key="default"
      from="*"
      to={firstRoute.slug}
    />,
  );

  return routeList;
};

class WebApp extends Component {
  constructor(props) {
    super(props);
    this.setLastLocation();
  }

  setLastLocation(location) {
    this.location = (this.location === undefined) ? null : location;
  }

  render() {
    return (
      <Router>
        <Route render={({ location }) => {
          const currentPathFirstSlug = location.pathname.split('/')[1];
          const lastPathFirstSlug = (this.location === null)
            ? null
            : this.location.pathname.split('/')[1];
          const thisLocation = (currentPathFirstSlug === lastPathFirstSlug)
            ? this.location
            : location;
          this.setLastLocation(thisLocation);
          return (

            <PageContext.Provider value={
              (pages[`/${thisLocation.pathname.split('/')[1]}`] !== undefined)
                ? pages[`/${thisLocation.pathname.split('/')[1]}`]
                : pages[Object.keys(pages)[0]]
              }
            >
              <Nav />

              <hr />

              <TransitionGroup>
                <CSSTransition key={thisLocation.pathname} classNames="fade" timeout={300}>
                  <Switch location={location}>
                    {getRouteList()}
                  </Switch>
                </CSSTransition>
              </TransitionGroup>

            </PageContext.Provider>

          );
        }}
        />
      </Router>
    );
  }
}

export default WebApp;
