import React from 'react';

import Page from '../page';

export const pages = {
  '/home': {
    component: Page,
    slug: '/home',
  },
  '/about': {
    component: Page,
    slug: '/about',
  },
  '/topics': {
    component: Page,
    slug: '/topics',
  },
  '/topics/:topicId': {
    component: Page,
    slug: '/topics/:topicId',
  },
};

export const PageContext = React.createContext(
  pages[Object.keys(pages)[0]],
);
